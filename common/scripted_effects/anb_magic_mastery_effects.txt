﻿#Resets magic mastery to 0
reset_magic_mastery_stat_counter = {
	set_variable = {
		name = magic_mastery
		value = 0
	}
}

#Resets magical lifestyle perk counter to 0
reset_magical_lifestyle_perk_counter = {
	set_variable = {
		name = magic_lifestyle_total_points
		value = 0
	}
}

#Updates magical lifestyle perk counter 
update_magical_lifestyle_perk_counter = {
	set_variable = {
		name = magic_lifestyle_total_points
		value = root.magic_lifestyle_perks
	}
}

#Master function for updating magic mastery
update_magic_mastery_stat = {
	reset_magic_mastery_stat_counter = yes
	update_magical_lifestyle_perk_counter = yes
	change_variable = {
		name = magic_mastery
		add = root.magic_lifestyle_perks
	}
}

magic_mastery_debug = {
	reset_magic_mastery_stat_counter = yes
	change_variable = {
		name = magic_mastery
		add = 100
	}
}