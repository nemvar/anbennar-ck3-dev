﻿lencori_group = {
	graphical_cultures = {
		frankish_group_coa_gfx
		western_coa_gfx
		mediterranean_building_gfx
		western_clothing_gfx
		western_unit_gfx
	}
	mercenary_names = {
	}
	

	lorenti = {
		
		color = "lorentish_green"
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {
			Adoyre Adrien Aedan Alan Alar Aldrien Alfrond Alor Alured Alwen Anaoc
			Aniud Anton Arnalt Arthel
			Austol Baldoen Bastian Beli Beltram
			Benabic Bernead Bledric Bledros Blethin Blethiut
			Bodan Bran Branhucar Branoc Brenci Breselcoucant Breselgar Breseloc Breselueu Brethoc
			Brient Brigualt Brioc Brithael Brochuael Bronmael Budguoret Budhoiarn Budic Budoc Buhedoc Butgual Cadan
			Cadaouen Cadarvan Cadiou Cadoc Cador Cadou Cantgueithen Caradec Caradoc Carantoc Catgualart
			Catuallon Catguethen Catguoret Caurantin Ceneu Cenguethen Cenmarhoc Cenmin
			Cenmonoc Cingur Cinhoedl Clodouan ClotwoI_on Comoere Conan Concen Condeleu Condidan Congar
			Conmael Conmonoc Conogan Conredeu Conuallon Conmarch Convelin ConwoI_on Corguethen Courantgern
			Culmin Dauid Deniel Denis Deroch Derrien Deui Deurhoiarn Domnoret Domuel
			Donual Donuallon Drogon Duenerth Dumnarth Edern Edouart Eguen Eon ErispoE_ Erouan
			Estienne Etgual Eudon Euhocar Euhoiarn Eusebi Even Evrard Farinmael Ferant Fracan
			Framual Fransaz Frioc Garmon Geofroi Geralt Gereint Gestin Gilbert Godefroi
			Goennoc Gouesnon Goulwen Gouremor Gourmaelon Gralon Gratcant Gregor Grifiud Guallon Gualter Gudual Guecon Guenerdon
			Guenhael Guentanet Guentiern Gui Guiharan Guihomarch Guihomart Guilherm Guincalon Guincum Guinhael Guinhoiarn Guinoc Guinol
			Guithrit Guitol Guobrian Guocon Guoedanau Guoedatoe Guoednou Guoethoiarn Guoletec Guorbili Guordotal Guoretan Guorethoiarn Guorhoiarn
			Gurbodu Gurcant Gurcencor Gurceneu Gurci Gurcon Gurdilic Gurgen Gurguaret Gurguist Gurguistl Gurheter
			Guriant Gurlouen Gurmael Gurmaeloc Gurmhaelon Gurthcid Gurthiern Guruant Guerech Gueroc Guethenan Guethencar Guethencor
			GuethenwoI_on Guethenhoiarn Guethenoc Haelcomarch Haelguethen Haelguoret Haelhoiarn Haeloc Haeloubri Haermael Hammelin
			Hamon Harscoit Hedyn Heranal Herri Hesdren Hincomhal Hinguethen Hocar Hoconan Honfroi HoE_l Huiarnal Huiarnbili
			Hunfroi Iacob Iarcun Iarnbud Iarncant Iarncar Iarndetguid Iarnegon Iarnuallon Iarnuocon Iarnhitin
			Iedechael Iehan Ilcarthon Ildut Iliud Inisian Iocilin Iodoc Ionas Iosep Iudcum Iuduallon Iudhael
			Iudicael Iudnerth Iudnimet Iustoc Iudoc Iudual Iumael Iunceneu Iunuocon Iunmonoc Jord Leucum
			Lorens Louenan Loumarch LouI_s Maelcon MaeldoI_ Maelhuedoc Maeloc Maelscoet Maelscuet Maenceneu Maenci
			March Marthin MatheudoI_ Mathias Matoc Matuid Maugan Mauric Medrod Meldroc Meriadoc
			Mermin Meugan Meven Michel Morcant Morcondeleu Morgen Morguethen Morhaetho Mormarch Morvan
			Mortiern Neven Nicolas Nodethael Nodhoiarn NominoE_ Nougui Oliuer Oswallt Padric Paol Pascueten
			Patern Phelippe Piran Prigent Primael Primarhoc Putrael PE_r Ralf Raoul Renan Resmen Riacet
			Ribald Ricart Riceneu Ridoredh Riualtr Rihouen Rinduran Rioc Riol Riwal Riuallon Rodric
			Roenualloc Roenuallon Roenuolou Roenhoiarn Rolland Ronan Roparth Roucen Rumon Ryd Salenn Salomon Samson
			Samuel Stefen Sulgen Sulhoiarn Sulmonoc Sultiern Talan Talhoiarn Tanci Tanet Tanhoiarn Telent Terithien Tethion Teudric
			Teudur Tibault Tiernan Tiernmael Tiernoc Tiernualloc Tiernuallon Treveur Tristan Tuduallon Tudur Tugdual
			Ungust Urbcen Urbien Urblon Urvod Uther Vincenne

			Caradaig Caruorst Castant Ciniod 

			#New
			Rewan Lorenan Floris Ruben
			Rewouen Lorentol Lorevarn Lorendal Flormael Florvod Floucen Ruven Rubitern Rubonas Rubiherm Runan Rugern
		}
		female_names = {
			Aanor Abrelda Adela AdelaI_da Adelindis Adeltrudis Aedoc Aeluit Agnes Alana Alis Anaguistl Anaudat Anaudrec
			Anauhouguel Anauprost Andrea Anna Aourcen Aouregan Aourell Aouren Argantan Argantdrec Arganteila Arganteilin Argantguistl Arganthael Argantlon
			Argantmoet Artaca Avandrec Avenie Beladora Berchet Berengaria Bernegarda Berta Berthildis Bertissa Bleutuen Brendana
			Bronuen Buriana Canna Cateline Catguistl Cecilia Ceindrec Ceinguled Cleirui Condeleu Constansa
			Cristen Dengel Deniela Denise Diuset Dreanau Drebiu Dreguoreth Drelouguen Drichglur Drilego Duihon Elara Eleanor Eleina
			Elena Elesbed Elisaued Elwen Emma Endellion Ennoguent Enoguen Enora Eonnecte Erouana Ewsannec Fuandrec Gaela
			Geberga Glades Gloiucen Gloiumed Guelet Guenbrith Guencen Guencenedl Guencor Guenguerthlon Guenguistl Guenguiu Guenmon Guenna Guenneret
			Guennguar Gueruel GuigoE_don Guillemecte Gwenn Halbiu Hawis Herannuen Hodierne Igerna Ignoguen Ilcum Ildegarda
			Iofa Iouenna Isabelle Iseult Iudhent Iudlouguen Iudprost Janed Judhael Languoret Leonore Leueer Leuhemel
			Leuoc Louhelic Luncen Maben Magdalene Marguarite Mari Medguistl Meduil Melita Meonre Milian Mona Morganne
			Morguetel Moruen Moruith Morvana Neulenna Nodguoret Nodiunet Nolwenne Nonne Oncenedl Oncum Onguen Ordguydel Ourcen Ourdilic Perenn
			Proscen Prosguetel Prostlon Rannoeu Riguistl Rimoet Rioanon Rioanon Rioantcen Rioantdrec Rioantel Rioantguellt Roenhael Roinoc
			Ronana Rozenn Rum Rumun Seva Stefania Steren Sulcar Sulcen Sulgubri Sulgwen Sulleisoc Sulmed Susanne Tandrec Tanetbiu Tanetlowen
			Tanguistl Tedvil Tephaine Thomine Tibourge Trifine Tristana Uriell
		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = caucasian_blond
			5 = caucasian_ginger
			45 = caucasian_brown_hair
			35 = caucasian_dark_hair
		}
	}

	lorentish = {
		graphical_cultures = {
			french_coa_gfx
		}
		color = "lorentish_red"

		cadet_dynasty_names = {
			{ "dynnp_of" "dynn_Capet" }
			"dynn_Fournier"
			{ "dynnp_du" "dynn_Dros" }
			{ "dynnp_de" "dynn_Piemont" }
			{ "dynnp_de" "dynn_Bage" }
			{ "dynnp_de" "dynn_Montferrat" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Anjou" }
		}

		dynasty_names = {
			"dynn_Fournier"
			{ "dynnp_du" "dynn_Dros" }
			{ "dynnp_de" "dynn_Piemont" }
			{ "dynnp_de" "dynn_Bage" }
			{ "dynnp_de" "dynn_Montferrat" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Anjou" }
			"dynn_Chamillart"
		}

		male_names = {
			Adrien Aedan Alan Alar Aldrien Alfrond Alor Alwen Anton Arnalt Arthel
			Bastian Beltram
			Benabic Bernead Bledric Bledros Blethin Blethiut
			Bodan Bran Branhucar Branoc
			Brient Brioc Cadan
			Cadaouen Cadarvan Cadoc Cador Caradec Caradoc Carantoc
			Deniel Denis Deroch Derrien Domuel
			Edern Edouart Eguen Erouan
			Even Evrard Farinmael Ferant Fracan
			Geofroi Gereint Gestin
			Hamon Hedyn Heranal Herri Hesdren
			Lorens Louenan Loumarch LouI_s Maelcon 
			Mauric
			Mermin Meugan Meven Michel Morcant Morvan
			Mortiern Neven Nicolas Nodethael Oliuer Padric Paol
			Patern Piran Prigent Ralf Raoul Renan Resmen Riacet
			Ribald Ricart Riceneu Rihouen Rinduran Rioc Riol Riuallon Rodric
			Rolland Ronan Roucen Rumon Salenn
			Samuel Stefen Sulgen Sultiern Talan
			Teudur Tibault Tiernan Tiernoc Treveur Tristan Tuduallon Tudur
			Urbien Uther Vincenne

			E_douard E_rrard E_tienne AdE_mar Adrien Aimery Alain Aldebert AldE_ric Alphonse Amaury
			AmE_dE_e Ancel AndrE_ Antoine Arnaud Arnoul Aubry Aymar BarthE_lE_mi
			Bouchard Centule
			Clotaire Eustache Evrard FrE_dE_ric GE_raud Gaucher Gaucelin
			Gauthier GE_raud Guichard Guiges Guilhem Guillaume
			HE_lie Hamelin Henri Herbert Hugues Jacques Jaspert Jean Josselin
			Jourdain Julien LE_on LE_onard Lothaire Mathieu Maurice Nicolas Ogier Onfroy Orson Othon
			Pierre Raoul Raymond Raynaud Renaud Robin Roger
			Rorgon Roubaud Savary Thibault ThiE_baut Thierry Thomas Valeran Yves

			#From EU4 - Anbennarian names
			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas Casten Castin Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto
			Rean Ricain Rión Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Lucian Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#New
			Rewan Lorenan Floris Ruben Adenn Kylian Perceval Gawain Geraint
		}
		female_names = {
			#From EU4 - Anbennarian names
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castina Cela Celadora Clarimonde Corina Cora Coraline Eilis Eilisabet Erella Erela Galina Galinda 
			Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille Kerstin Alisanne Isobel 
			Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilie Gisele Athana

			#New
			Kylia

			Abrelda Adela AdelaI_da Aeluit Agnes Alana Alis
			Andrea Anna Aourcen Aouren
			Artaca Avenie Beladora Berchet Bertissa Bleutuen Brendana
			Bronuen Buriana Canna Cateline Cecilia Ceinguled Condeleu Constansa
			Cristen Deniela Denise Elara Eleina
			Elena Elwen Emma Ennoguent Enoguen Enora 
			Iofa Iouenna Isabelle Iseult Iudhent Iudlouguen Iudprost Janed Judhael Languoret Leonore Leueer Leuhemel
			Leuoc Maben Magdalene Marguarite Mari Meduil Mona Morganne
			Moruen Moruith Morvana Neulenna Nodguoret Nodiunet Nolwenne Nonne 
			Ronana RozennSeva Stefania Steren Sulcar Sulcen Susanne
			Tephaine Thomine Trifine Tristana Uriell

			E_lE_onore E_lodie AdE_le Adelaide Adelinde Agathe AgnE_s AlE_arde Alice AliE_nor
			Alix Almodis Amelie Anne Antoinette Arsinde Aude Aurengarde BE_atrice BE_atrix BenoI_te
			Berthe Blanche Bonne CE_cile CathE_rine Charlotte Constance Denise Douce Echive Eglantine
			Emma Esclarmonde Euphrosine Eustachie Eve Gerberge
			GisE_le Guillaumette HE_loise Helvis Hodierne Ide Ida IldE_garde Isabeau Isabelle Jeanne Judith
			Julienne Mafalda Mahaut Margot Marguerite Marie Marthe Mascarose Mathilde ME_lisande
			ME_lisende ME_lusine PE_ronelle Pernelle Perinne Pernette Plaisance Raymonde Sarrazine SolE_ne Sophie
			StE_phanie Sybille Tiburge Valence Yolande
		}

		dynasty_of_location_prefix = "dynnp_sil"
		grammar_transform = french

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 60
		mat_grf_name_chance = 10
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 60
		mother_name_chance = 10
		
		ethnicities = {
			5 = caucasian_blond
			30 = caucasian_ginger
			60 = caucasian_brown_hair
			5 = caucasian_dark_hair
		}
		
		mercenary_names = {
			{ name = "mercenary_company_tard_venus_1" }
			{ name = "mercenary_company_french_band_1" }
			{ name = "mercenary_company_routiers_1" }
			{ name = "mercenary_company_french_band_2" }
		}
	}
	
	roilsardi = {
		graphical_cultures = {
			french_coa_gfx
		}
		color = "roilsardi_red"

		cadet_dynasty_names = {
			{ "dynnp_of" "dynn_Capet" }
			"dynn_Fournier"
			{ "dynnp_du" "dynn_Dros" }
			{ "dynnp_de" "dynn_Piemont" }
			{ "dynnp_de" "dynn_Bage" }
			{ "dynnp_de" "dynn_Montferrat" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Anjou" }
		}

		dynasty_names = {
			"dynn_Fournier"
			{ "dynnp_du" "dynn_Dros" }
			{ "dynnp_de" "dynn_Piemont" }
			{ "dynnp_de" "dynn_Bage" }
			{ "dynnp_de" "dynn_Montferrat" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Anjou" }
			
		}

		male_names = {
			1 = {
				Adrien Aedan Alan Alar Aldrien Alfrond Alor Alwen Anton Arnalt Arthel
				Bastian Beltram
				Benabic Bernead Bledric Bledros Blethin Blethiut
				Bodan Bran Branhucar Branoc
				Brient Brigualt Brioc Cadan
				Cadaouen Cadarvan Cadoc Cador Caradec Caradoc Carantoc
				Deniel Denis Deroch Derrien Domuel
				Edern Edouart Eguen Erouan
				Even Evrard Farinmael Ferant Fracan
				Geofroi Gereint Gestin
				Hamon Hedyn Heranal Herri Hesdren
				Iacob Iarcun
				Lorens Louenan Loumarch LouI_s Maelcon 
				Mauric
				Mermin Meugan Meven Michel Morcant Morvan
				Mortiern Neven Nicolas Nodethael Oliuer Padric Paol
				Patern Piran Prigent Ralf Raoul Renan Resmen Riacet
				Ribald Ricart Riceneu Rihouen Rinduran Rioc Riol Riuallon Rodric
				Rolland Ronan Roucen Rumon Salenn
				Samuel Stefen Sulgen Sulmonoc Sultiern Talan
				Teudur Tibault Tiernan Tiernoc Treveur Tristan Tuduallon Tudur
				Urbien Uther Vincenne

				E_douard E_rrard E_tienne AdE_mar Adrien Aimery Alain Aldebert AldE_ric Alphonse Amaury
				AmE_dE_e Ancel AndrE_ Antoine Arnaud Arnoul Aubry Aymar BarthE_lE_mi
				Bouchard Centule
				Clotaire Eustache Evrard FrE_dE_ric GE_raud Gaucher Gaucelin
				Gauthier GE_raud Guichard Guiges Guilhem Guillaume
				HE_lie Hamelin Henri Herbert Hugues Jacques Jaspert Jean Josselin
				Jourdain Julien LE_on LE_onard Lothaire Mathieu Maurice Nicolas Ogier Onfroy Orson Othon
				Pierre Raoul Raymond Raynaud Renaud Robin Roger
				Rorgon Roubaud Savary Thibault ThiE_baut Thierry Thomas Valeran Yves

				#From EU4 - Anbennarian names
				Adran Aldred Alfons Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Calas Casten Castin Corin Daran Darran Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto
				Rean Ricain Rión Rogier Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Dominic Vernell Arman Cecill Tristan Alvar Adelar Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil
			}
			#New
			5 = { Lucian Petrus Gregoire Alaric Ambrose Damien Silas Stefan Filip Armand }	#more common proper roilsardi names
		}
		female_names = {
			1 = {
				Abrelda Adela AdelaI_da Aeluit Agnes Alana Alis
				Andrea Anna Aourcen Aouren
				Artaca Avenie Beladora Berchet Bertissa Bleutuen Brendana
				Bronuen Buriana Canna Cateline Cecilia Ceinguled Condeleu Constansa
				Cristen Deniela Denise Elara Eleina
				Elena Elwen Emma Ennoguent Enoguen Enora 
				Iofa Iouenna Isabelle Iseult Iudhent Iudlouguen Iudprost Janed Judhael Languoret Leonore Leueer Leuhemel
				Leuoc Maben Magdalene Marguarite Mari Meduil Mona Morganne
				Moruen Moruith Morvana Neulenna Nodguoret Nodiunet Nolwenne Nonne 
				Ronana RozennSeva Stefania Steren Sulcar Sulcen Susanne
				Tephaine Thomine Trifine Tristana Uriell

				E_lE_onore E_lodie AdE_le Adelaide Adelinde Agathe AgnE_s AlE_arde Alice AliE_nor
				Alix Almodis Amelie Anne Antoinette Arsinde Aude Aurengarde BE_atrice BE_atrix BenoI_te
				Berthe Blanche Bonne CE_cile CathE_rine Charlotte Constance Denise Douce Echive Eglantine
				Emma Esclarmonde Euphrosine Eustachie Eve Gerberge
				GisE_le Guillaumette HE_loise Helvis Hodierne Ide Ida IldE_garde Isabeau Isabelle Jeanne Judith
				Julienne Mafalda Mahaut Margot Marguerite Marie Marthe Mascarose Mathilde ME_lisande
				ME_lisende ME_lusine PE_ronelle Pernelle Perinne Pernette Plaisance Raymonde Sarrazine SolE_ne Sophie
				StE_phanie Sybille Tiburge Valence Yolande

				#From EU4 - Anbennarian names
				Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castina Cela Celadora Clarimonde Corina Cora Coraline Eilis Eilisabet Erella Erela Galina Galinda 
				Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille Kerstin Alisanne Isobel 
				Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilie Gisele Athana
			}

			#New
			5 = { Lilith Luciana Petrona Gregora Alarique Ambrosia Damiana Stefana Filipa Katerine Katrina }
		}

		dynasty_of_location_prefix = "dynnp_sil"
		grammar_transform = french

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 60
		mat_grf_name_chance = 10
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 60
		mother_name_chance = 10
		
		ethnicities = {
			20 = caucasian_ginger
			65 = caucasian_brown_hair
			15 = caucasian_dark_hair
		}
		
		mercenary_names = {
			{ name = "mercenary_company_tard_venus_1" }
			{ name = "mercenary_company_french_band_1" }
			{ name = "mercenary_company_routiers_1" }
			{ name = "mercenary_company_french_band_2" }
		}
	}
	
	derannic = {
		graphical_cultures = {
			norman_coa_gfx
		}
		
		color = "derannic_pink"

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Villeneuve" }
			"dynn_Balliol"
			"dynn_Plantagenet"
			{ "dynnp_de" "dynn_Chypre" }
			{ "dynnp_de" "dynn_Harenc" }
			{ "dynnp_de" "dynn_Tiberiade" }
			{ "dynnp_de" "dynn_Montjoy" }
			{ "dynnp_de" "dynn_Toron" }
			{ "dynnp_d_" "dynn_Anjou" }
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Villeneuve" }
			"dynn_Balliol"
			"dynn_Plantagenet"
	
		}


		male_names = {
			#From EU4 - Anbennarian names
			Adran Aldred Alfons Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Calas Casten Castin Corin Daran Darran Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto
			Rean Ricain Rión Rogier Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Dominic Vernell Arman Cecill Tristan Alvar Adelar Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#New
			Adenn Artus Tomar Mathos Kalbard Daromger Daroman Daromdan

			#From Vanilla
			Anders Anund Asger Edvard Einar Ernst Esben Gotfred Gregers
			Halvor Harald Helge Holger Ingolf Joakim
			Ludvig Malthe Olav Rolf Sigfred
			Sivard Valdemar 

			Abelard Adelard Ademar Alfons Anselm Armand Arnald
			Bernard Berold Bertran Botolf Errand Faramond Ferant Filbert
			Fulk Gaillard Gaufrid Gerald Gerard GE_rard Geraud Gerbert Gervas Giffard Gilbert
		    Guntard Herman Hubert Hugh Humphrey 
		    Lambert Mauger Orderic Osbern Osmond Oswulf Oswyn Radolf Radulf Randolf Ranulf Raymond Rayner
		    Robert Rolland Roscelin Sayer Serlo Serril Sewal Stefen Tancred Turold Turstin
		    Tybalt Walter William Wymund
	   }
	   female_names = {
			#From EU4 - Anbennarian names
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castina Cela Celadora Clarimonde Corina Cora Coraline Eilis Eilisabet Erella Erela Galina Galinda 
			Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille Kerstin Alisanne Isobel 
			Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilie Gisele Athana

			#New
			Calassa 

			#From Vanilla
			Adelaide Adele Adelin Adelise Agnes Alberada Alienora Aline Alisce Alison Amburga Aubrey Aveis Avelina Berengaria
			Busilla Clarimond Constance Egelina Elise Elysande Emelenine Emma Emonie Eremberga Ermyntrude
			Felicia Flandina Fredesende Fressenda Fressende Geva Giselle Gohilde Griselda Gundred Hadwis Helewisa Heria Herleve Hermessent Hermine Hugolina
			Ida Judit Lesceline Mabel Marie Matheode Matilda Maud Mautild Maysant Millicent Muriella Oriel Richenda
			Rosalind Selova Stephanie Sibyl Sibylla Umfreda Valdrade Wilmot Yolanda Yvon

			Agnethe Anna Asta Benedikte Birgitta Bodil Cecilie Dagmar Dorothea Ellinor Emilie Erika Ester Estrid Freja
			Grethe Gudrun Gunhild Gyda Helena Ida Ingegerd Ingrid Johanne Jutta Karin Karoline Katarina Katrine Klara Lise Luitgard Lykke
			Magdalene Maren Margrethe Maria Martha Rikissa Runa Sanna Signe Sigrid Sofie
	   }
		
		dynasty_of_location_prefix = "dynnp_sil"
		grammar_transform = french
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5

		ethnicities = {
			25 = caucasian_blond
			10 = caucasian_ginger
			30 = caucasian_brown_hair
			15 = caucasian_northern_blond
			5 = caucasian_northern_ginger
			15 = caucasian_northern_brown_hair
		}

		mercenary_names = {
			{ name = mercenary_company_band_of_the_black_monk }
			{ name = mercenary_company_iron_arms_company }
			{ name = mercenary_company_normans_of_drengot }
		}
	}

	sorncosti = {
		graphical_cultures = {
			occitan_coa_gfx
		}
		
		color = { 135 3 86 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Rodez" }
			{ "dynnp_de" "dynn_Bergerac" }
			{ "dynnp_de" "dynn_L_isle_Jourdain" }
			{ "dynnp_de" "dynn_Poitiers-Valentinois" }
			"dynn_Trencval"
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Rodez" }
			{ "dynnp_de" "dynn_Bergerac" }
			{ "dynnp_de" "dynn_L_isle_Jourdain" }
			{ "dynnp_de" "dynn_Poitiers-Valentinois" }
			"dynn_Trencval"
			{ "dynnp_de" "dynn_ChA_tellerault" }
			{ "dynnp_de" "dynn_Rancon" }
			{ "dynnp_de" "dynn_Guyenne" }
			{ "dynnp_de" "dynn_Bezieres" }
			{ "dynnp_de" "dynn_Arelat" }
			{ "dynnp_de" "dynn_Ponthieu" }
			{ "dynnp_de" "dynn_Provence" }
			{ "dynnp_de" "dynn_Toulouse" }
			{ "dynnp_d_" "dynn_Aquitaine" }
			{ "dynnp_de" "dynn_PontevE_s" }
			{ "dynnp_de" "dynn_Sabran" }
			{ "dynnp_de" "dynn_Crussol" }
			{ "dynnp_de" "dynn_Quatrebarbes" }
			{ "dynnp_de" "dynn_la_Panouse" }
		}

		male_names = {

			#From EU4 - Anbennarian names
			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas Casten Castin Corin Daran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto
			Rean Ricain Rión Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Lucian Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#New
			Alcuin Alcar Darran Petran Cavin

			#From Vanilla - Occitan
			AdhE_mar Acfred Aimeric Alberic Aldebert AnfO_s Amalric Ancelmes AndrE_ Arnaut Artau Aton AudoI_n Aymar Aymeric
			Barral BartoumiE_u Beneset BE_rard Bermond Bernat Borel Boson Carles
			ClamenC_ Duran EmmanuE_l Enric EstE_ve Filip
			Ferrand FlorE_nC_ Frederi Gaston Gausbert Gautier GilbE_rt Girard Girvais Godafres
			Gui Guichard GuilhE_m Guiraud Guitart Ives Jacme Jorge
			LaurE_nC_ LoI_s Loui Lop MarC_au Martin Matfre Matias Milo Miquel
			Nicholaus Odon Otton PE_ire Peranudet Pol Pons Raimond Rainaut Rainer
			Raolf Ricard RobE_rt Rogier Savarics TiE_rri Tibaud Toumas Ubald Ubert
			VE_zian VicenC_
		}
		female_names = {
			#From EU4 - Anbennarian names
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castina Cela Celadora Clarimonde Corina Cora Coraline Eilis Eilisabet Erella Erela Galina Galinda 
			Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille Kerstin Alisanne Isobel 
			Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilie Gisele Athana

			AdE_la AdalaI_da Adeltrudis Agata AI_na AinE_s AlaI_s AlienO_r AliC_ AlmO_dis AlpaI_s Ana Arsenda Assalhida Ava
			AzalaI_s Azelma BO_na Beatritz Berenguela Berta Blanca Brandimena Bregida Brunissenda Camila CarlO_ta Caterina Cecilia
			Clara Clarmonda ClemE_ncia Cloutilda ConstA_ncia DolC_a EisabE_u Elena
			Elisa ElisabE_ta Ermengarda Ermessentz Esclarmonda Estefania EufE_mia Eufrosina
			Faidida Filipa Filomena FlO_ra Francesa Garcenda Geneviva GerbE_rga Gersenda Gisla Guigone Guilheumina Heloise
			Ioulanda IsabE_u IsabE_la Joana Juliana LU_cia Laura LoI_sa Mabila Madalena
			Margarida Maria Marquisa Marta MascarO_sa Matilda MelisE_nda Navar Patricia PeironE_la
			Petronilha Puelle Raisenda Raimonda RichE_nda RosE_la Rosa SanC_a Sibilla TerE_sa Tiborg Verounica

			#New
			Laurenne Fiona Fionca
		}

		dynasty_of_location_prefix = "dynnp_sil"
		grammar_transform = french

		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			10 = caucasian_blond
			5 = caucasian_ginger
			35 = caucasian_brown_hair
			50 = caucasian_dark_hair
		}


		mercenary_names = {
			{ name = "mercenary_company_routiers_of_the_archpriest" }
			{ name = "mercenary_company_host_of_the_viscount" }
			{ name = "mercenary_company_marchers_of_wasconia" }
		}
	}

	sormanni = {
		graphical_cultures = {
			mediterranean_building_gfx
			occitan_coa_gfx
		}
		
		color = { 135 3 86 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Rodez" }
			{ "dynnp_de" "dynn_Bergerac" }
			{ "dynnp_de" "dynn_L_isle_Jourdain" }
			{ "dynnp_de" "dynn_Poitiers-Valentinois" }
			"dynn_Trencval"
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Rodez" }
			{ "dynnp_de" "dynn_Bergerac" }
			{ "dynnp_de" "dynn_L_isle_Jourdain" }
			{ "dynnp_de" "dynn_Poitiers-Valentinois" }
			"dynn_Trencval"
		}

		male_names = {

	
			#New
			Alcuin Alcar Darran Petran Cavin

			#From Vanilla - Occitan
			AdhE_mar Acfred Aimeric Alberic Aldebert AnfO_s Amalric Ancelmes AndrE_ Arnaut Artau Aton AudoI_n Aymar Aymeric
			Barral BartoumiE_u Beneset BE_rard Bermond Bernat Borel Boson Carles
			ClamenC_ Duran EmmanuE_l Enric EstE_ve Filip
			Ferrand FlorE_nC_ Frederi Gaston Gausbert Gautier GilbE_rt Girard Girvais Godafres
			Gui Guichard GuilhE_m Guiraud Guitart Ives Jacme Jorge
			LaurE_nC_ LoI_s Loui Lop MarC_au Martin Matfre Matias Milo Miquel
			Nicholaus Odon Otton PE_ire Peranudet Pol Pons Raimond Rainaut Rainer
			Raolf Ricard RobE_rt Rogier Savarics TiE_rri Tibaud Toumas Ubald Ubert
			VE_zian VicenC_
		}
		female_names = {

			AdE_la AdalaI_da Adeltrudis Agata AI_na AinE_s AlaI_s AlienO_r AliC_ AlmO_dis AlpaI_s Ana Arsenda Assalhida Ava
			AzalaI_s Azelma BO_na Beatritz Berenguela Berta Blanca Brandimena Bregida Brunissenda Camila CarlO_ta Caterina Cecilia
			Clara Clarmonda ClemE_ncia Cloutilda ConstA_ncia DolC_a EisabE_u Elena
			Elisa ElisabE_ta Ermengarda Ermessentz Esclarmonda Estefania EufE_mia Eufrosina
			Faidida Filipa Filomena FlO_ra Francesa Garcenda Geneviva GerbE_rga Gersenda Gisla Guigone Guilheumina Heloise
			Ioulanda IsabE_u IsabE_la Joana Juliana LU_cia Laura LoI_sa Mabila Madalena
			Margarida Maria Marquisa Marta MascarO_sa Matilda MelisE_nda Navar Patricia PeironE_la
			Petronilha Puelle Raisenda Raimonda RichE_nda RosE_la Rosa SanC_a Sibilla TerE_sa Tiborg Verounica

		}

		dynasty_of_location_prefix = "dynnp_of"
		grammar_transform = french
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			50 = caucasian_brown_hair
			50 = caucasian_dark_hair
		}


		mercenary_names = {
			{ name = "mercenary_company_routiers_of_the_archpriest" }
			{ name = "mercenary_company_host_of_the_viscount" }
			{ name = "mercenary_company_marchers_of_wasconia" }
		}
	}
	
	iochander = {
		
		color = { 173 61 135 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {
			#From EU4 - Anbennarian names
			Adran Aldred Alfons Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Calas Casten Castin Corin Daran Darran Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto
			Rean Ricain Rión Rogier Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Dominic Vernell Arman Cecill Tristan Alvar Adelar Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#New
			Adenn Artus Tomar Mathos Kalbard Daromger Daroman Daromdan

			#From Vanilla
			Anders Anund Asger Edvard Einar Ernst Esben Gotfred Gregers
			Halvor Harald Helge Holger Ingolf Joakim
			Ludvig Malthe Olav Rolf Sigfred
			Sivard Valdemar 

			Abelard Adelard Ademar Alfons Anselm Armand Arnald
			Bernard Berold Bertran Botolf Errand Faramond Ferant Filbert
			Fulk Gaillard Gaufrid Gerald Gerard GE_rard Geraud Gerbert Gervas Giffard Gilbert
		    Guntard Herman Hubert Hugh Humphrey 
		    Lambert Mauger Orderic Osbern Osmond Oswulf Oswyn Radolf Radulf Randolf Ranulf Raymond Rayner
		    Robert Rolland Roscelin Sayer Serlo Serril Sewal Stefen Tancred Turold Turstin
		    Tybalt Walter William Wymund
		}
		
	    female_names = {
			#From EU4 - Anbennarian names
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castina Cela Celadora Clarimonde Corina Cora Coraline Eilis Eilisabet Erella Erela Galina Galinda 
			Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille Kerstin Alisanne Isobel 
			Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilie Gisele Athana

			#New
			Calassa 

			#From Vanilla
			Adelaide Adele Adelin Adelise Agnes Alberada Alienora Aline Alisce Alison Amburga Aubrey Aveis Avelina Berengaria
			Busilla Clarimond Constance Egelina Elise Elysande Emelenine Emma Emonie Eremberga Ermyntrude
			Felicia Flandina Fredesende Fressenda Fressende Geva Giselle Gohilde Griselda Gundred Hadwis Helewisa Heria Herleve Hermessent Hermine Hugolina
			Ida Judit Lesceline Mabel Marie Matheode Matilda Maud Mautild Maysant Millicent Muriella Oriel Richenda
			Rosalind Selova Stephanie Sibyl Sibylla Umfreda Valdrade Wilmot Yolanda Yvon

			Agnethe Anna Asta Benedikte Birgitta Bodil Cecilie Dagmar Dorothea Ellinor Emilie Erika Ester Estrid Freja
			Grethe Gudrun Gunhild Gyda Helena Ida Ingegerd Ingrid Johanne Jutta Karin Karoline Katarina Katrine Klara Lise Luitgard Lykke
			Magdalene Maren Margrethe Maria Martha Rikissa Runa Sanna Signe Sigrid Sofie
	   	}
		

		dynasty_of_location_prefix = "dynnp_sil"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			30 = caucasian_blond
			10 = caucasian_ginger
			50 = caucasian_brown_hair
			10 = caucasian_dark_hair
		}
	}
}