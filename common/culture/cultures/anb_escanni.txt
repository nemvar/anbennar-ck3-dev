﻿escanni_group = {
	graphical_cultures = {
		frankish_group_coa_gfx
		western_coa_gfx
		western_building_gfx
		western_clothing_gfx
		western_unit_gfx
		dde_hre_clothing_gfx
	}
	mercenary_names = {
	}

	adenner = {
		
		color = { 15 117 187 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	

			#EU4 - castanorian+elvenized
			Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
			Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
			Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
			Caylen Arman

			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
			Casten Castín Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
			Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery 
			Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#From Vanilla
			Alaric Clotaire
			Bertold Bertram
			Kaspar Konrad Arnold Arnulf
			Avin Bernhard
			Willem Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chlothar
			Osric Sigeric Eadric

			#New
			Garren

		}
		female_names = {
			#EU4
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castína Cela Celadora Clarimonde Corina Cora Coraline Eilís Eilísabet Erella Erela Galina 
			Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
			Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilíe Gisele Athana

			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Bertrada Bertilla Gisela Ida Sophia
			Helene Judith Ida Susanna
			Claudia Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Reynilde Maria Mathilde

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = slavic_blond
			5 = slavic_ginger
			55 = slavic_brown_hair
			30 = slavic_dark_hair
		}
	}
	
	white_reachmen = {
		graphical_cultures = {
           northern_clothing_gfx
        }
		
		color = { 250 250 250 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	#added some north germanic names

			#EU4
			Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Clothar Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Elecast Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
			Caylen Armann

			#From Vanilla
			Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod
			Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger
			Otto Kaspar Konrad Rothard
			Avin Bernhard Osnath Wambald
			Willem Steyn Alwin Barend Egmund Hugo Roland
			
			Ulric Ulfric 
			Anund Alv Arne Arnfinn Arnkjell Arnmod Arnvid Aslak Audun Balder BA_rd BjOErn Eigil Eilif Einar Eirik Erlend Erling Eystein Finn Frej
			Grim Gudbrand Gudleik Gudmund GudrOEd Gunnar Guttorm Haldor Halfdan Halkjell Harald Helge HA_kon HA_vard Inge Ivar Jon KA_re Kolbein Lodin
			Magnus Odd Ogmund Olav Orm Ossor Ottar PAAl Ragnar Ragnvald Rolf Sigurd Skjalg Skofte Skule Svein Sverre SA_mund TorbjOErn Tord Tore
			Torbrand Torfinn Torgeir Torgil
			Tormod Torolf Torstein Trond Tryggve Ulv Vigleik A_le A_mund O_lver O_ystein
		}

		female_names = {
			#EU4
			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
			Helene Hemma Judith Ida Susanna
			Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

			Anna Astrid Brigida Cecilia Eldrid Freja Gjertrud Gudrid Gudrun Gunnhild Gyda Gyrid Haldora Homlaug IngebjO_rg Ingjerd Ingrid Jorunn
			Karin Kristina Margrete Maria Martha Ragna Ragnfrid Ragnhild Rannveig Sigrid SnO_frid Sunniva Svanhild Thora Thorborg Thordis A_se A_shild A_sta

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			45 = slavic_blond
			25 = slavic_northern_blond
			5 = slavic_ginger
			5 = slavic_northern_ginger
			10 = slavic_brown_hair
			10 = slavic_dark_hair
		}
	}
	
	black_castanorian = {
		graphical_cultures = {
           northern_clothing_gfx
		   western_clothing_gfx
        }
		
		color = { 25 25 25 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	#added some north germanic names

			#EU4
			Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Clothar Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Elecast Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
			Caylen Armann

			#From Vanilla
			Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod
			Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger
			Otto Kaspar Konrad Rothard
			Avin Bernhard Osnath Wambald
			Willem Steyn Alwin Barend Egmund Hugo Roland
			
			Ulric Ulfric 
			Anund Alv Arne Arnfinn Arnkjell Arnmod Arnvid Aslak Audun Balder BA_rd BjOErn Eigil Eilif Einar Eirik Erlend Erling Eystein Finn Frej
			Grim Gudbrand Gudleik Gudmund GudrOEd Gunnar Guttorm Haldor Halfdan Halkjell Harald Helge HA_kon HA_vard Inge Ivar Jon KA_re Kolbein Lodin
			Magnus Odd Ogmund Olav Orm Ossor Ottar PAAl Ragnar Ragnvald Rolf Sigurd Skjalg Skofte Skule Svein Sverre SA_mund TorbjOErn Tord Tore
			Torbrand Torfinn Torgeir Torgil
			Tormod Torolf Torstein Trond Tryggve Ulv Vigleik A_le A_mund O_lver O_ystein

			#New
			Garren Garrec Castant 
		}

		female_names = {
			#EU4
			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
			Helene Hemma Judith Ida Susanna
			Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

			Agatha Cynehild Ecgwyn Octreda

			Anna Astrid Brigida Cecilia Eldrid Freja Gjertrud Gudrid Gudrun Gunnhild Gyda Gyrid Haldora Homlaug IngebjO_rg Ingjerd Ingrid Jorunn
			Karin Kristina Margrete Maria Martha Ragna Ragnfrid Ragnhild Rannveig Sigrid SnO_frid Sunniva Svanhild Thora Thorborg Thordis A_se A_shild A_sta

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			45 = slavic_blond
			15 = slavic_northern_blond
			20 = slavic_brown_hair
			10 = slavic_northern_brown_hair
			10 = slavic_dark_hair
			5 = slavic_northern_dark_hair
		}
	}

	marcher = {
		
		color = { 230 216 166 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	#bit of central german names here

			#EU4
			Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Elecast Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
			Caylen Arman Adrien Adrian

			#From Vanilla
			Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod Alarich Clotaire
			Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger Hulderic Humfried Theoderic Theodoric
			Otto Kaspar Konrad Arnold Arnulf Rothard
			Avin Bernhard Meginulf Osnath Wambald
			Willem Steyn Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chararic Charibert Childebert Childeric Chilperic Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat

			#New
			Garren Garrec Castant 
		}

		female_names = {
			#EU4
			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
			Helene Hemma Judith Ida Susanna
			Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

			Agatha Cynehild Ecgwyn Octreda

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			50 = slavic_blond
			40 = slavic_brown_hair
			10 = slavic_dark_hair
		}
	}

	castellyrian = {
		
		color = { 144 144 200 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	

			#EU4 - castanorian+elvenized
			Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
			Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
			Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
			Caylen Arman

			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
			Casten Castín Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
			Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Lucian 
			Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#From Vanilla
			Alaric Clotaire
			Bertold Bertram
			Kaspar Konrad Arnold Arnulf
			Avin Bernhard
			Willem Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chlothar

			#New
			Garren

		}
		female_names = {
			#EU4
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castína Cela Celadora Clarimonde Corina Cora Coraline Eilís Eilísabet Erella Erela Galina 
			Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
			Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilíe Gisele Athana

			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Bertrada Bertilla Gisela Ida Sophia
			Helene Judith Ida Susanna
			Claudia Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Reynilde Maria Mathilde

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			20 = slavic_blond
			5 = slavic_ginger
			35 = slavic_brown_hair
			40 = slavic_dark_hair
		}
	}

	castanite = {
		
		color = { 255 255 255 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#Names of Ancient Castanor ALL have a C in there
			
			#From EU4
			Acromar Marcan Arcan Dolmec Melcor Melcon Bracan Dorcan Aucan Canrec Camir Camor Carlan Celgal Clothar Coreg Crovan Crovis Corrac
			Devac Elecast Castor Castorn Godrac Gracos Humac Madalac Ottrac Rabac Rogec Stovac Tomac Tormac 
			Ulric #from Gerudia
			Venac Bellac 

			#From Vanilla
			Castantin Carus Crassus Gordian Alaric Clodion Osric Morcar

			#New
			Suracos Cravoc Cravosian Maldec Tarcan Nerac Ceran Acalos Alacan Cragan Octavos

		}
		female_names = {
			Claranda Alica Catrana Arcana Melcora Marcana Bracana Camira Camora Carlana Celga Cora Macilda Castora Godraca Graca Madaca Ottraca Rogeci Stovaci Camaco
			Venaca Bellaca Clamorna Ecena Clametra Cerene Cerena Zencara Octava Castantina Carusa Clodia Acroma

			Clara Clothilde Brethoc Cothilda
			Claudia Candida Clementia
		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = african	#ancient castanite roots
			10 = east_african
			15 = arab
			15 = mediterranean_byzantine
									#TODO: Castanite Pureblood pale black hair (arannese)
			20 =  slavic_brown_hair
			30 = slavic_dark_hair
		}

	}

	castanorian = {
		
		color = { 200 200 200 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	#bit of central german names here

			#EU4
			Acromar Adelar Adrian Adrien Alain Alen Arman Arnold Bellac Camir Camor Canrec Carlan Caylen Celgal Ciramod Clarimond Clothar
			Colyn Corac Coreg Corric Crovan Crovis Dalyon Delan Delian Devac Devan Dustyn Edmund Elecast Frederic Godrac Godric
			Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard
			Rican Ricard Rogec Roger Rycan Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan

			#From Vanilla
			Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod Alarich Clotaire
			Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger Hulderic Humfried Theoderic Theodoric
			Otto Kaspar Konrad Arnold Arnulf Rothard
			Avin Bernhard Meginulf Osnath Wambald
			Willem Steyn Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chararic Charibert Childebert Childeric Chilperic Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat

			Osric Morcar Sigeric Eadric

			#New
			Garren Garrec Castant 
		}

		female_names = {
			#EU4
			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
			Helene Hemma Judith Ida Susanna
			Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

			Agatha Cynehild Ecgwyn Octreda
		}



		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			1 = african	#ancient castanite roots
			1 = east_african
			2 = arab
			11 = byzantine

			10 = slavic_blond
			5 = slavic_ginger
			30 = slavic_brown_hair
			30 = slavic_dark_hair
		}

	}

	rohibonic = {
		
		color = { 18 84 130 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	#bit of central german names here

			#EU4
			Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
			Elecast Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
			Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
			Caylen Arman Adrien Adrian

			#From Vanilla
			Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod Alarich Clotaire
			Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger Hulderic Humfried Theoderic Theodoric
			Otto Kaspar Konrad Arnold Arnulf Rothard
			Avin Bernhard Meginulf Osnath Wambald
			Willem Steyn Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chararic Charibert Childebert Childeric Chilperic Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat
			Osric Morcar Sigeric Eadric

			#New
			Garren Garrec Castant 
		}

		female_names = {
			#EU4
			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
			Helene Hemma Judith Ida Susanna
			Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

			Agatha Cynehild Ecgwyn Octreda

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = slavic_blond
			5 = slavic_ginger
			55 = slavic_brown_hair
			30 = slavic_dark_hair
		}

	}

	urionmari = {
		
		color = { 163 218 255 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	

			#EU4 - castanorian+elvenized
			Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
			Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
			Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
			Caylen Arman

			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
			Casten Castín Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
			Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery 
			Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#From Vanilla
			Alaric Clotaire
			Bertold Bertram
			Kaspar Konrad Arnold Arnulf
			Avin Bernhard
			Willem Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chlothar
			Osric Sigeric Eadric

			#New
			Garren

		}
		female_names = {
			#EU4
			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castína Cela Celadora Clarimonde Corina Cora Coraline Eilís Eilísabet Erella Erela Galina 
			Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
			Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilíe Gisele Athana

			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			#From Vanilla
			Bertrada Bertilla Gisela Ida Sophia
			Helene Judith Ida Susanna
			Claudia Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Reynilde Maria Mathilde

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = slavic_blond
			35 = slavic_brown_hair
			55 = slavic_dark_hair
		}

	}

	calindalic = {
		
		color = { 255 216 76 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {	

			#EU4 - castanorian+elvenized
			Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
			Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
			Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
			Caylen Arman

			Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
			Casten Castín Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
			Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery 
			Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

			#From Vanilla
			Alaric Clotaire
			Bertold Bertram
			Kaspar Konrad Arnold Arnulf
			Avin Bernhard
			Willem Alwin Barend Egmund Hugo Roland Leuderic
			Ceslin Chlothar
			Osric Sigeric Eadric

			#New
			Garren

		}
		female_names = {
			#EU4
			Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
			Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
			Coraline Cora Corina Cecille Valence Athana

			Adra Alara Alina Amarien Aria Calassa Aucanna Castennia Castína Cela Celadora Clarimonde Corina Cora Coraline Eilís Eilísabet Erella Erela Galina 
			Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
			Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence Emilíe Gisele Athana

			#From Vanilla
			Bertrada Bertilla Gisela Ida Sophia
			Helene Judith Ida Susanna
			Claudia Christine Ursula
			Alena Fenna Elke 
			Filippa Frida Reynilde Maria Mathilde

		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			50 = slavic_blond
			40 = slavic_brown_hair
			10 = slavic_dark_hair
		}

	}
}